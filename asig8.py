from flask import Flask,render_template,request,redirect,url_for,flash
from flask_mysqldb import MySQL
from celery import Celery
import smtplib
app = Flask(__name__)

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


app.config['MYSQL_HOST']='127.0.0.1'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='password'
app.config['MYSQL_DB']='wordsflask'
mysql = MySQL(app)


@app.route('/')
def Index():
    curl = mysql.connection.cursor()
    curl.execute('SELECT * FROM asig7')
    data = curl.fetchall()
    print(data)
    return render_template('index.html',asig7=data)

@app.route('/add_word',methods=['POST'])
def add_word():
    if request.method == 'POST':
        PALABRA = request.form['PALABRA']
        SIGNIFICADO = request.form['SIGNIFICADO1']
    
        EnviarCorreo.delay(PALABRA,SIGNIFICADO)
        curl = mysql.connection.cursor()
        curl.execute('INSERT INTO asig7 (PALABRA,SIGNIFICADO1) VALUES (%s,%s)',(PALABRA,SIGNIFICADO1))
        mysql.connection.commit()
        
    flash('La Palabra ha sido agregada exitosamente!!')    
    return redirect(url_for('Index'))

@app.route('/edit/<ID>')
def edit_word(ID):
    curl = mysql.connection.cursor()
    curl.execute('SELECT * FROM asig7 WHERE ID = %s',(ID))
    data = curl.fetchall()

    return render_template('edit.html',PALABRA = data[0])

@app.route('/update/<ID>', methods = ['POST'])
def update_word(ID):
    if (request.method =='POST'):
        PALABRA = request.form['PALABRA']
        SIGNIFICADO1 = request.form['SIGNIFICADO1']
        curl = mysql.connection.cursor()
        curl.execute('UPDATE asig7 SET PALABRA = %s,SIGNIFICADO1 = %s WHERE ID = %s ',(PALABRA,SIGNIFICADO1,ID))
        mysql.connection.commit()
        flash('La Palabra ha sido actualizada e')
    return redirect(url_for('Index'))

@app.route('/delete/<string:ID>')
def delete_word(ID):
    curl = mysql.connection.cursor()
    curl.execute('DELETE FROM asig4 WHERE ID = {0}'.format(ID))
    mysql.connection.commit()
    flash('La Palabra ha sido removida ')
    return redirect(url_for('Index'))

@celery.task
def EnviarCorreo(p1, p2):

    message = 'Palabra agregada {} Con su Significado {} '.format(p1,p2)
    subject="Una Nueva Palabra ha sido integrada!"
    print(message)
    message= 'Subject: {}\n\n{}'.format(subject,message)
    server = smtplib.SMTP('smtp.gmail.com',587)
    
    server.starttls()
    
    server.login('asig7juliov@gmail.com','jv507')
    
    server.sendmail('asig7juliov@gmail.com','vasquezjulio2100@gmail.com',message)
    
    server.quit()    
    print('El correo ha sido enviado')
    return message

if __name__ == "__main__":
    app.run(host='0.0.0.0')
